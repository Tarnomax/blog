from django.shortcuts import render
from .models import Post, Categoria
from django.db.models import Q
from django.core.paginator import Paginator

def home (request):
    queryset = request.GET.get("buscar")
    posts = Post.objects.filter(estado = True)
    if queryset:
        posts= Post.objects.filter(
            Q(titulo__icontains = queryset) |
            Q(descripcion__icontains = queryset)
        ).distinct()

    paginator = Paginator(posts,2)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    return render(request,'index.html',{'posts': posts})

def detallePost(request,slug):
    post= Post.objects.get(slug = slug)
    return render(request,'post.html', {'detalle_post': post})


def generales(request):
    posts = Post.objects.filter(estado = True, categoria = Categoria.objects.get(nombre = 'Generales'))

    paginator = Paginator(posts,2)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    return render(request,'generales.html', {'posts': posts})

def programacion(request):
    posts = Post.objects.filter(estado = True, categoria = Categoria.objects.get(nombre = 'Programacion'))
    paginator = Paginator(posts,2)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    return render(request,'programacion.html',{'posts': posts})

def tutoriales(request):
    posts = Post.objects.filter(estado = True, categoria = Categoria.objects.get(nombre = 'Tutoriales'))
    paginator = Paginator(posts,2)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    return render(request,'tutoriales.html', {'posts': posts})

def tecnologia(request):
    posts = Post.objects.filter(estado = True, categoria = Categoria.objects.get(nombre = 'Tecnologia'))
    paginator = Paginator(posts,2)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    return render(request,'tecnologia.html',{'posts': posts})

def videoJuegos(request):
    posts = Post.objects.filter(estado = True, categoria = Categoria.objects.get(nombre = 'Videojuegos'))
    paginator = Paginator(posts,2)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    return render(request,'videoJuegos.html',{'posts': posts})
