from django.apps import AppConfig


class BlogBlueserviceConfig(AppConfig):
    name = 'blog_blueservice'
