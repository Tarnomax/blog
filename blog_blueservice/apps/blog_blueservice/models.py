from django.db import models
from ckeditor.fields import RichTextField

class Categoria (models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField('Nombre de la categoria', max_length=100, null= False, blank=False)
    estado = models.BooleanField('Categoria Activida/Categoria no activada', default=True)
    fecha_creacion = models.DateField('Fecha de cracion', auto_now= False , auto_now_add=True)

    class Meta :
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'

    def __str__(self):
        return self.nombre
    
class Autor (models.Model):
    id = models.AutoField(primary_key=True)
    nombres = models.CharField('Nombres de autores', max_length= 255 ,null= False, blank= False)
    apellidos = models.CharField('Apellidos de autores', max_length=255 , null= False, blank=False)
    facebook = models.URLField('Facebook', null= True, blank=True)
    instagram  = models.URLField('instagram', null= True, blank=True)
    web = models.URLField('web', null= True, blank=True)
    email = models.EmailField('Correo electronico',null= False, blank = False)
    estado = models.BooleanField('Autor activo/ Autor inactivo', default= True)
    fecha_creacion = models.DateField('Fecha de cracion', auto_now= False , auto_now_add=True)

    class Meta:
        verbose_name = 'Autor'
        verbose_name_plural = 'Autores'

    def __str__(self):
        return "{0},{1}".format(self.apellidos, self.nombres)


class Post (models.Model):
    id = models.AutoField(primary_key=True)
    titulo = models.CharField('título', max_length= 90)
    slug = models.SlugField('slug', max_length=100)
    descripcion = models.CharField('Descripcion', max_length= 110)
    contenido = RichTextField()
    imagen = models.URLField(max_length=250)
    autor = models.ForeignKey(Autor, on_delete = models.CASCADE)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    estado = models.BooleanField('Publicado / No publicado', default=True)
    fecha_creacion = models.DateField('Fecha de creación', auto_now= False, auto_now_add=True)

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'

    def __str__(self):
        return self.titulo
    


